const qulp = require('gulp');
const { task } = require('gulp');
const minifycss = require("gulp-minify-css");
const rename = require("gulp-rename");
const concat = require('gulp-concat');
// const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();

const jsfiles = [
    './src/js/vendors/jquery.min.1.7.js',
    './src/js/vendors/modernizr.2.5.3.min.js',
    './src/js/vendors/turnjs/*.js',
    './src/js/app/*.js',
    './src/js/index.js',
];

const cssfiles = [
    './src/css/**/*.css',
];

function build() {
    return qulp.src(jsfiles)
        // .pipe(sourcemaps.init()) 
        .pipe(concat('app' + '.js'))
        .pipe(rename({suffix: ".min"}))
        // .pipe(sourcemaps.write('.'))
        .pipe(qulp.dest("./dist"));
};

function buildcss() {
    return qulp.src(cssfiles)
        .pipe(concat('app.css'))
        .pipe(rename({suffix: ".min"}))
        .pipe(minifycss({keepSpecialComments : 0}))
        .pipe(qulp.dest("./dist"));
}

task('watch', () => {
    qulp.watch([
        'src/js/index.js',
        'src/js/app/**/*.js',
        './src/css/basic.css',
    ])
    .on('change', qulp.parallel(build, buildcss));
});

task(build);
task(buildcss);

task('serve', function(){
    browserSync.init({
        server: "dist"
    });
    browserSync.watch('dist/**/*.*').on('change', browserSync.reload );
});

task('dev',
    qulp.series(
        'build',
        'buildcss',
        qulp.parallel('watch', 'serve')
    )
);