class Page {
    constructor(image) {
        this.image = image;
    }

    render() {
        return `<div style="background-image:url(${this.image})"></div>`
    }
}