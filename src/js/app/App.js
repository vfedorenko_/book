class App {
	constructor() {
		this.book = new Book(PAGES);
	}

	init = () => {
		this.book.render();
		this.render();
		this.loadApp();
		this.book.init();
	}

    loadApp = () => {
		const flipbook = $('.flipbook');
        flipbook.turn({
			width: 922,
			height: 600,
			elevation: 50,
			gradients: true,
			autoCenter: true
        });
	}
	
	onHoverEvent = () => {
		let show = document.querySelector('.fullScreenBtn');
		show.classList.remove('hide');
	}

	onBlurEvent = () => {
		let show = document.querySelector('.fullScreenBtn');
		show.classList.add('hide');
	}

	fullScreenRequest = () => {
		const container = document.querySelector('.container');
		const fullScreenBtn = document.querySelector('.fullScreenBtn');

		container.classList.add('fullScreen');
		if (!document.fullscreenElement) {
			container.requestFullscreen();
		}

		fullScreenBtn.removeEventListener('click', this.fullScreenRequest);
		fullScreenBtn.addEventListener('click', this.fullScreenExit);
	}

	fullScreenExit = () => {
		const container = document.querySelector('.container');
		const fullScreenBtn = document.querySelector('.fullScreenBtn');

		container.classList.remove('fullScreen');
		if (document.fullscreenElement) {
			document.exitFullscreen();
		}


		fullScreenBtn.removeEventListener('click', this.fullScreenExit);
		fullScreenBtn.addEventListener('click', this.fullScreenRequest);
	}

	render = () => {
		const { 
			onHoverEvent, 
			onBlurEvent, 
			fullScreenRequest,
		} = this;

		const container = document.querySelector('.container');
		container.innerHTML += '<div ignore="1" class="fullScreenBtn hide"></div>';
		container.addEventListener('mouseover', onHoverEvent);
		container.addEventListener('mouseout', onBlurEvent);

		const fullScreenBtn = document.querySelector('.fullScreenBtn');
		fullScreenBtn.addEventListener('mouseover', onHoverEvent);
		fullScreenBtn.addEventListener('click', fullScreenRequest);
	}
};
