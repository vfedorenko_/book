class Book {
    constructor(pages) {
        this.pages = pages || [];
    }

    init = () => {
        this.prevButton();
		this.nextButton();
    }

    addPage = (child) => {
        let page = new Page(child);
        return page.render();
    }

    prevPage = ( e ) => {
        e.preventDefault();
        console.log('prev');
        $('.flipbook').turn('previous');
    }

    nextPage = ( e ) => {
        e.preventDefault();
        console.log('next');
        $('.flipbook').turn('next');
    }

    prevButton = () => {
        console.log('prevButton')
        $('.prev-button').bind($.mouseEvents.over, function() {
            $(this).addClass('button-hover');
        }).bind($.mouseEvents.out, function() {
            $(this).removeClass('button-hover');
        }).click( this.prevPage );
    }

    nextButton = () => {
        console.log('nextButton')
        $('.next-button').bind($.mouseEvents.over, function() {
            $(this).addClass('button-hover');
        }).bind($.mouseEvents.out, function() {
            $(this).removeClass('button-hover');
        }).click( this.nextPage );
    }

    render() {
        const { addPage } = this;
        
        const container = document.querySelector('.container');
        const flipbook = document.createElement('div');
        flipbook.className = 'flipbook';

        let html = `<button ignore="1" class="prev-button"></button>`;
        if (this.pages.length > 0) {
            this.pages.forEach( item => {
                html += addPage(item);
            } );
        };
        html += `<button ignore="1" class="next-button"></button>`;

        flipbook.innerHTML = html;
        container.appendChild(flipbook);
    }
}